function selectYear(){
	let max = new Date().getFullYear(),
		min = max - 120,
		select = document.getElementById('year');
	for (let i = max; i>=min; i--){
		let opt = document.createElement('option');
		opt.value = i;
		opt.innerHTML = i;
		select.appendChild(opt);
	}
}
function selectDay(){
	let max = 31;
		min = 1;
		select = document.getElementById('date');
	for (let i = min; i<=max; i++){
		let opt = document.createElement('option');
		opt.value = i;
		opt.innerHTML = i;
		select.appendChild(opt);
	}
}
selectYear();
selectDay();
function nameValidate(name){
	let letters = /^[A-Z a-z]+$/;
	if (name.value){
		if(name.value.match(letters))
		{
			return true;
		}
		else
		{
			document.getElementById("error").innerHTML = "Invalid Name: Name should only have Alphabats";
			document.getElementById("result").innerHTML = "";
		return false;
		}
	}
	else{
		document.getElementById("error").innerHTML = "Invalid Name: Please Enter the Name";
		document.getElementById("result").innerHTML = "";
	}
 
}
function validateDate(year,month,date,currentDate){
	const MONTHLENGTH = [31,28,31,30,31,30,31,31,30,31,30,31];
	if ((year)&&(month)&&(date)){
		if ((year==currentDate.getFullYear())&&(month>currentDate.getMonth()+1)){
			document.getElementById("error").innerHTML = "Invalid Birthday";
			document.getElementById("result").innerHTML = "";
		}
		else if((year==currentDate.getFullYear())&&(month==currentDate.getMonth()+1)&&(date>currentDate.getDate())){
			document.getElementById("error").innerHTML = "Invalid Birthday";
			document.getElementById("result").innerHTML = "";
		}
		else{
			if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)){
				MONTHLENGTH[1] = 29;
			}
			if(date > 0 && date <= MONTHLENGTH[month - 1]){
				return true;
			}
			else{
				document.getElementById("error").innerHTML = "Invalid Date";
				document.getElementById("result").innerHTML = "";
				return false;
			}
		}
	}
	else{
		document.getElementById("error").innerHTML = "Invalid Date: Please Enter the Birthday";
		document.getElementById("result").innerHTML = "";
	}	
}
function calculateAge(){
	const YEAR = parseInt(document.getElementById("year").value),
		MONTH =parseInt(document.getElementById("month").value),
		DATE = parseInt(document.getElementById("date").value),
		NAME = document.getElementById("name");
	const NOW = new Date();
	if (validateDate(YEAR,MONTH,DATE,NOW)&&nameValidate(NAME)){
		let age = NOW.getFullYear() - YEAR;
		let monthAge = NOW.getMonth() - MONTH + 1; 
		if (monthAge < 0) {
			--age;
			monthAge+=12;
		}
		else if (monthAge == 0) {
			let daysAge = NOW.getDate() - DATE;
			if (daysAge < 0) --age;
		}
		document.getElementById("result").innerHTML = NAME.value+" is "+ age+" Years "+monthAge+" months Old";
		document.getElementById("error").innerHTML = "";	
	}	 
}



